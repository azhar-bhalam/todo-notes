package id.pentacode.retrofit_gson_example;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

import id.pentacode.retrofit_gson_example.adapter.ToDoAdapter;
import id.pentacode.retrofit_gson_example.helper.CustomRVItemTouchListener;
import id.pentacode.retrofit_gson_example.helper.RecyclerViewItemClickListener;
import id.pentacode.retrofit_gson_example.models.ToDo;
import id.pentacode.retrofit_gson_example.reposervice.ApiService;
import id.pentacode.retrofit_gson_example.retrofit.RetroClient;
import id.pentacode.retrofit_gson_example.utils.InternetConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements AdapterClick {

    private ArrayList<ToDo> contactList;
    private List<Integer> delList = new ArrayList<>();
    private Button btnDelete;
    ToDoAdapter adapter;
    RecyclerView recyclerContacts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnDelete = findViewById(R.id.btnDelete);
        loadContactsData();

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("RRRR",delList.size()+"");
                if(delList.size()>0){
                    for (int i=0; i<delList.size(); i++){
                        contactList.remove(delList.get(i).intValue());
                    }
//                    adapter.notifyDataSetChanged();
                    adapter = new ToDoAdapter(contactList, MainActivity.this);
                    recyclerContacts.setAdapter(adapter);
                }
            }
        });
    }

    protected void loadContactsData() {

        /**
         * Checking Internet Connection
         */
        if (InternetConnection.checkConnection(getApplicationContext())) {

            final ProgressDialog dialog;

            /**
             * Progress Dialog for User Interaction
             */
            dialog = new ProgressDialog(MainActivity.this);
            dialog.setTitle("Getting JSON data");
            dialog.setMessage("Please wait...");
            dialog.show();

            //Creating an object of our api interface
            ApiService api = RetroClient.getApiService();

            /**
             * Calling JSON
             */
            Call<ArrayList<ToDo>> call = api.getToDos();

            /**
             * Enqueue Callback will be call when get response...
             */
            call.enqueue(new Callback<ArrayList<ToDo>>() {
                @Override
                public void onResponse(Call<ArrayList<ToDo>> call, Response<ArrayList<ToDo>> response) {
                    //Dismiss Dialog
                    dialog.dismiss();

                    if(response.isSuccessful()) {

                        /**
                         * Got Successfully
                         */
                        // Log.d("LOGGG", "onResponse: " + response.body().getContacts());
                        contactList = response.body();

                        /**
                         * Binding that List to Adapter
                         */
                        recyclerContacts = (RecyclerView) findViewById(R.id.recyclerContacts);
                        adapter = new ToDoAdapter(contactList, MainActivity.this);
                        recyclerContacts.setAdapter(adapter);
                        recyclerContacts.setLayoutManager(new LinearLayoutManager(MainActivity.this));

                        /**
                         * Add listener to every recycler view items
                         */
                        recyclerContacts.addOnItemTouchListener(new CustomRVItemTouchListener(MainActivity.this, recyclerContacts, new RecyclerViewItemClickListener() {
                            @Override
                            public void onClick(View view, int position) {

                                Snackbar.make(findViewById(R.id.layoutMain), "onClick at position : " + position, Snackbar.LENGTH_LONG).show();

                            }

                            @Override
                            public void onLongClick(View view, int position) {
                                Snackbar.make(findViewById(R.id.layoutMain), "onLongClick at position : " + position, Snackbar.LENGTH_LONG).show();
                            }
                        }));

                    } else {
                        Snackbar.make(findViewById(R.id.layoutMain), "Something going wrong!", Snackbar.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<ToDo>> call, Throwable t) {
                    dialog.dismiss();
                }
            });

        } else {
            Snackbar.make(findViewById(R.id.layoutMain), "Check your internet connection!", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onchkClick(int position) {
        Log.i("DDD",position+"");
        if(delList.contains(position)){
            delList.remove(position);
        }else {
            delList.add(position);
        }
    }
}
