package id.pentacode.retrofit_gson_example.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import id.pentacode.retrofit_gson_example.R;

public class ToDoHolder extends RecyclerView.ViewHolder {

    public CardView cardContact;
    public TextView txtName;
    public TextView txtEmail;
    public TextView tvCompleted;
    public CheckBox cbDel;

    public ToDoHolder(View itemView) {
        super(itemView);
        cardContact = (CardView) itemView.findViewById(R.id.cardContact);
        txtName = (TextView) itemView.findViewById(R.id.txtName);
        txtEmail = (TextView) itemView.findViewById(R.id.txtEmail);
        tvCompleted = (TextView) itemView.findViewById(R.id.tvCompleted);
        cbDel = (CheckBox) itemView.findViewById(R.id.cbDel);
    }
}
