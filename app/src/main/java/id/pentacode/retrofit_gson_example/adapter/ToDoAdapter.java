package id.pentacode.retrofit_gson_example.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.Collections;
import java.util.List;

import id.pentacode.retrofit_gson_example.AdapterClick;
import id.pentacode.retrofit_gson_example.R;
import id.pentacode.retrofit_gson_example.holder.ToDoHolder;
import id.pentacode.retrofit_gson_example.models.ToDo;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoHolder> {

    List<ToDo> list = Collections.emptyList();
    Context context;

    public ToDoAdapter(List<ToDo> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ToDoHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_todo_row_layout, parent, false);

        return new ToDoHolder(view);
    }

    @Override
    public void onBindViewHolder(ToDoHolder holder, final int position) {

        // Use the provided View Holder on the onCreateViewHolder method to populate the current row on the RecyclerView
        holder.txtName.setText(list.get(position).getTitle());
        try{
            holder.txtEmail.setText("ID is : "+list.get(position).getId());
        }catch (Exception e){
            Log.i("DONERR",e.getLocalizedMessage());
        }
        holder.tvCompleted.setText(" "+list.get(position).getCompleted());
        holder.cbDel.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ((AdapterClick)context).onchkClick(position);
            }
        });

        // animate(holder);
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    // Insert a new item to the RecyclerView on a predefined position
    public void insert(int position, ToDo data) {
        list.add(position, data);
        notifyItemInserted(position);
    }

    // Remove a RecyclerView item containing a specified Data object
    public void remove(ToDo data) {
        int position = list.indexOf(data);
        list.remove(position);
        notifyItemRemoved(position);
    }

}
